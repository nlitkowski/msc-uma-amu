import numpy as np


# 1.1


def kwadraty(input_list):
    output_list = [x*x for x in input_list if x > 0]
    return output_list


# 1.2

# print(np.arange(1, 51).reshape(5, 10))

# 1.3


def wlasciwosci_macierzy(A):
    liczba_elementow = A.size
    liczba_wierszy, liczba_kolumn = A.shape
    srednie_wg_wierszy = A.mean(axis=1)
    srednie_wg_kolumn = A.mean(axis=0)
    kolumna_2 = A[:, 2]
    wiersz_3 = A[3, :]
    return (
        liczba_elementow, liczba_kolumn, liczba_wierszy,
        srednie_wg_wierszy, srednie_wg_kolumn,
        kolumna_2, wiersz_3)

# 1.4


def dzialanie1(A, x):
    """ iloczyn macierzy A z wektorem x """
    return A.dot(x)


def dzialanie2(A, B):
    """ iloczyn macierzy A · B """
    return np.matmul(A, B)


def dzialanie3(A, B):
    """ wyznacznik det(A · B) """
    return np.linalg.det(np.matmul(A, B))


def dzialanie4(A, B, x):
    """ wynik działania (A · B)^T - B^T · A^T """
    return np.subtract(((np.matmul(A, B)).T), (np.matmul(B.T, A.T)))


# A = np.array([[-412, -2], [5, 5], [2, 2]])
# B = np.array([[-44, -11], [6, 5213], [224, 3]]).T
# x = np.array([[31245, 4]]).T

# print("A:\n{}".format(A))
# print("B:\n{}".format(B))
# print("x:\n{}".format(x))

# print("mnozenie z wektorem:\n{}".format(dzialanie1(A, x)))
# print("mnozenie z macierza:\n{}".format(dzialanie2(A, B)))
# print("wyznacznik A*B:\n{}".format(dzialanie3(A, B)))
# print("wynik działania (A · B)^T - B^T · A^T:\n{}".format(dzialanie4(A, B, x)))

# 1.5

# A = np.array([5, 4, 7, 2], dtype=float).reshape(2, 2)
# print(A)
# A_mat = np.matrix(A)
# print(A_mat)
# print(A_mat**-1)
# print(A**-1)


# 1.6
X = np.array([[11, -2, 4], [54, 5, 7]])
y = np.array([[7, 23]]).T
X_m = np.matrix(X)
y_m = np.matrix(y)

o = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X)), X.T), y)
om = ((((X_m.T * X)**-1)*X_m.T)*y_m)

print("Array:\n{}".format(om))
print("Matrix:\n{}".format(o))
