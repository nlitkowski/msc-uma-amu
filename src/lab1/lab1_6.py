import numpy as np


X = np.array([[11, -2, 4], [54, 5, 7]])
y = np.array([[7, 23]]).T
X_m = np.matrix(X)
y_m = np.matrix(y)

res = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X)), X.T), y)
res_m = ((((X_m.T * X)**-1)*X_m.T)*y_m)

print("type 'array':\n{}".format(res))
print("type 'matrix':\n{}".format(res_m))
