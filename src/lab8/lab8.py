# Regresja przy użyciu algorytmu SGD
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd


def preprocess(data: pd.DataFrame):
    """Wstępne przetworzenie danych"""
    return data


def batch_iterate(x, y, batch_size):
    """Iterator dzielący dane na mini-batche"""
    assert len(x) == len(y)
    dataset_size = len(x)
    current_index = 0
    while current_index < dataset_size:
        x_batch = x[current_index: current_index + batch_size]
        y_batch = y[current_index: current_index + batch_size]
        yield x_batch, y_batch
        current_index += batch_size


# Nazwy plików
dataset_filename = 'mushrooms.tsv'

# Wczytanie danych
data = pd.read_csv(dataset_filename, header=None, sep='\t')
columns = data.columns
# data = data  # wybór cech
data = preprocess(data)  # wstępne przetworzenie danych

# Podział danych na zbiory uczący i testowy
split_point = int(0.8 * len(data))
data_train = data[:split_point]
data_test = data[split_point:]

print(data_train)
x_train = pd.DataFrame(data_train[1:])
y_train = pd.DataFrame(data_train[0]).to_numpy().ravel()

# # Skalowanie danych
# scaler = StandardScaler()
# x_train_scaled = scaler.fit_transform(x_train)

# Mini-batch SGD
model = SGDRegressor()
batch_iterator = batch_iterate(x_train, y_train, batch_size=100)
for x_batch, y_batch in batch_iterator:
    model.partial_fit(x_batch, y_batch)

# Predykcja wyników dla danych testowych
x_test = pd.DataFrame(data_test[1:])
y_expected = pd.DataFrame(data_test[0]).to_numpy().ravel()
y_predicted = model.predict(x_test)  # predykcja wyników na podstawie modelu

error = mean_squared_error(y_expected, y_predicted)

print(f"Błąd średniokwadratowy wynosi {error}")
