import numpy as np
import matplotlib.pyplot as plt


def main():
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.set_xlabel('x')
    ax.set_ylabel('y')

    x = np.arange(-2.5, 2.5, 0.001)

    # y = f(x) = (a−4)x^2 + (b−5)x + (c−6)
    # student index nr: 440054
    # therefore a = 0, b = 5, c = 6
    # outcome: y = f(x) = -4x^2 - 2
    y1 = -4 * x**2 - 2

    y2 = (np.e ** x) / ((np.e ** x) + 1)

    ax.plot(x, y1, color='blue', lw=2)
    ax.plot(x, y2, color='green', lw=2)

    plt.show()


if __name__ == "__main__":
    main()
