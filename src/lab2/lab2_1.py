import matplotlib.pyplot as plt
import pandas as pd


def main():
    data = pd.read_csv('data2.csv', sep=',')
    data_array = data.to_numpy()

    x = data_array[:, 5]
    y = data_array[:, 13]

    plt.plot(x, y, 'b^')
    plt.show()


if __name__ == "__main__":
    main()
