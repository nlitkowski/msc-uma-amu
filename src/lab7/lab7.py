import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
from tensorflow.keras.layers import Input, Dense, Activation, Dropout
from tensorflow.keras.models import Model
from keras.models import Sequential
from sklearn.metrics import mean_squared_error
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing
from sklearn.preprocessing import MinMaxScaler


def main():
    flats = pd.read_csv(
        "flats.tsv",
        sep="\t",
    )
    flats_preprocessed = check_outliers(flats)
    flats_preprocessed = normalize(flats_preprocessed)
    (train_dataset, test_dataset) = split_dataset(flats_preprocessed)
    res = fit_models(train_dataset, test_dataset)
    evaluate(res)


def check_outliers(flats):
    flats_preprocessed = flats[[
        "cena", "Powierzchnia w m2", "Stan", "Liczba pokoi"]]
    numberOfSqrMax = 0
    for sqrMeters in flats_preprocessed['Powierzchnia w m2']:
        if sqrMeters > 500:
            numberOfSqrMax += 1

    flats_preprocessed = flats_preprocessed[flats_preprocessed['Powierzchnia w m2'] < 500]

    numberOfPriceMax = 0
    for price in flats_preprocessed['cena']:
        if price > 1000000:
            numberOfPriceMax += 1

    flats_preprocessed = flats_preprocessed[flats_preprocessed['cena'] < 1000000]

    numberOfPriceMin = 0
    for price in flats_preprocessed['cena']:
        if price < 10000:
            numberOfPriceMin += 1

    flats_preprocessed = flats_preprocessed[flats_preprocessed['cena'] > 10000]

    fig, ax = plt.subplots(2, 2)
    fig.set_figheight(15)
    fig.set_figwidth(20)
    ax[0, 0].boxplot(flats_preprocessed['cena'])
    ax[0, 0].set_title('Price')
    ax[0, 1].boxplot(flats_preprocessed['Powierzchnia w m2'])
    ax[0, 1].set_title('Area in m2')
    ax[1, 1].boxplot(flats_preprocessed['Liczba pokoi'])
    ax[1, 1].set_title('Number of rooms')

    fig.show()

    return flats_preprocessed


def normalize(flats_preprocessed):
    flats_preprocessed.dropna()
    flats_preprocessed = pd.get_dummies(
        flats_preprocessed, columns=["Stan"], prefix="", prefix_sep=""
    )

    df_min_max_scaled = flats_preprocessed.copy()
    df_min_max_scaled['Powierzchnia w m2'] = (df_min_max_scaled['Powierzchnia w m2'] - df_min_max_scaled['Powierzchnia w m2'].min()) / (
        df_min_max_scaled['Powierzchnia w m2'].max() - df_min_max_scaled['Powierzchnia w m2'].min())
    df_min_max_scaled['Liczba pokoi'] = (df_min_max_scaled['Liczba pokoi'] - df_min_max_scaled['Liczba pokoi'].min()) / (
        df_min_max_scaled['Liczba pokoi'].max() - df_min_max_scaled['Liczba pokoi'].min())

    flats_preprocessed = df_min_max_scaled

    return flats_preprocessed


def split_dataset(flats_preprocessed):
    train_dataset = flats_preprocessed.sample(frac=0.8, random_state=0)
    test_dataset = flats_preprocessed.drop(train_dataset.index)

    sns.pairplot(
        train_dataset[["cena", "Powierzchnia w m2", "Liczba pokoi"]],
        diag_kind="kde",
    )

    plt.show()
    return (train_dataset, test_dataset)


def fit_models(train_dataset, test_dataset):
    train_features = train_dataset.copy()
    test_features = test_dataset.copy()
    train_labels = train_features.pop("cena")
    test_labels = test_features.pop("cena")

    model = tf.keras.Sequential([layers.Dense(units=1)])
    model.compile(
        optimizer=tf.optimizers.Adam(learning_rate=0.1), loss="mean_absolute_error"
    )

    history = model.fit(
        train_features,
        train_labels,
        epochs=30,
        verbose=0,
        validation_split=0.2,
    )
    test_results = {}
    test_results['First model'] = model.evaluate(
        test_features, test_labels, verbose=0)

    model_updated = keras.Sequential([
        layers.Dense(64, activation='relu'),
        layers.Dense(64, activation='relu'),
        layers.Dense(1)])

    model_updated.compile(loss='mean_absolute_error',
                          optimizer=tf.keras.optimizers.Adam(0.001))
    history = model_updated.fit(
        train_features, train_labels,
        validation_split=0.2,
        verbose=0, epochs=100)
    test_results['Second model'] = model_updated.evaluate(
        test_features, test_labels, verbose=0)

    return test_results


def evaluate(res):
    print(pd.DataFrame(res, index=['MAE for price']).T)


if __name__ == "__main__":
    main()
