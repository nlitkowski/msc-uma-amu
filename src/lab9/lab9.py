import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
from sklearn.preprocessing import StandardScaler


def main():
    # Wczytanie danych
    data = pd.read_csv('flats_for_clustering.tsv', header=[0], sep='\t')
    data.columns = [f'x{i+1}' for i in range(5)]

    data.loc[data.x5 == "poddasze", 'x5'] = -2
    data.loc[data.x5 == "parter", 'x5'] = 0
    data.loc[data.x5 == "niski parter", 'x5'] = -1

    data.dropna(inplace=True)

    data = data.astype('float64')

    X = data.values
    Xs = data.values[:, 2:4]

    Ys, history = k_means(Xs, 5)
    plot_clusters(Xs, Ys, 5, centroids=history[-1][0])
    X_pca = pca(X, 2)
    plot_unlabeled_data(X_pca)


def euclidean_distance(x1, x2):
    return np.linalg.norm(x1 - x2)


def k_means(X, k, distance=euclidean_distance):
    history = []
    Y = []

    centroids = [[random.uniform(X.min(axis=0)[f], X.max(axis=0)[f])
                  for f in range(X.shape[1])]
                 for _ in range(k)]

    while True:
        distances = [[distance(centroids[c], x) for c in range(k)] for x in X]
        Y_new = [d.index(min(d)) for d in distances]
        if Y_new == Y:
            break
        Y = Y_new
        XY = np.asarray(np.concatenate((X, np.matrix(Y).T), axis=1))
        Xc = [XY[XY[:, 2] == c][:, :-1] for c in range(k)]
        centroids = [[Xc[c].mean(axis=0)[f] for f in range(X.shape[1])]
                     for c in range(k)]
        history.append((centroids, Y))

    result = history[-1][1]
    return result, history


def plot_clusters(X, Y, k, centroids=None):
    color = ['saddlebrown', 'darkturquoise',
             'yellowgreen', 'cornflowerblue', 'darkmagenta', 'crimson']
    fig = plt.figure(figsize=(16*.7, 9*.7))
    ax = fig.add_subplot(111)
    fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
    plt.title('Klastry')

    X1 = X[:, 0].tolist()
    X2 = X[:, 1].tolist()
    X1 = [[x for x, y in zip(X1, Y) if y == c] for c in range(k)]
    X2 = [[x for x, y in zip(X2, Y) if y == c] for c in range(k)]

    for c in range(k):
        ax.scatter(X1[c], X2[c], c=color[c], marker='o', s=25, label='Dane')
        if centroids:
            ax.scatter([centroids[c][0]], [centroids[c][1]],
                       c=color[c], marker='+', s=500, label='Centroid')

    ax.set_xlabel(r'$x_1$')
    ax.set_ylabel(r'$x_2$')
    ax.margins(.05, .05)
    plt.show()


def pca(X, k):
    X_std = StandardScaler().fit_transform(X)
    cov_mat = np.cov(X_std.T)
    n = cov_mat.shape[0]
    eig_vals, eig_vecs = np.linalg.eig(cov_mat)
    eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i])
                 for i in range(len(eig_vals))]
    eig_pairs.sort()
    eig_pairs.reverse()
    matrix_w = np.hstack([eig_pairs[i][1].reshape(n, 1)
                          for i in range(k)])
    return X_std.dot(matrix_w)


def plot_unlabeled_data(X, col1=0, col2=1):
    fig = plt.figure(figsize=(16*.7, 9*.7))
    ax = fig.add_subplot(111)
    fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
    plt.title('Wykres danych')
    X1 = X[:, col1].tolist()
    X2 = X[:, col2].tolist()
    ax.scatter(X1, X2, c='k', marker='o', s=50, label='Dane')
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.margins(.05, .05)
    plt.show()


if __name__ == "__main__":
    main()
